#include "textparser.h"
#include "maths.h"
#include <regex>
#include <iostream> //debug

using namespace rpncalc;

long double rpncalc::converttodouble(std::string * input)
{
	long double toreturn;
	std::stringstream(*input) >> toreturn;
	return toreturn;
};

std::string rpncalc::converttostring(long double input)
{
	std::stringstream epic;
	epic << input;
	return epic.str();
};

int rpncalc::stackextract(std::stack<std::string> * mainstack, char** argv, int argc)
{
	for (unsigned int i = argc -1; i > 0; i--)
	{
		mainstack->push(argv[i]);
	}
	return 0;
};

bool rpncalc::isnumber(std::string * input)//this checks if it's a number
{
	if(std::regex_match(*input, std::regex("[0-9]")))
		return 1;
	return 0;
}

int rpncalc::workthrough(std::stack<std::string> * mainstack)
{
	while(!(mainstack->size() == 1)) //while there is more than one thing in the stack
	{
		long double toptwo[2] = {0,0}; //i can't remember how to initalise properly but its getting overwritten anyway so it makes no difference
		if(!isnumber(&mainstack->top()))
			return 1; //if the first number in this pair isn't a number then the format is wrong
		toptwo[0] = converttodouble(&mainstack->top());
		mainstack->pop();//cleared the first one
		std::cout << isnumber(&mainstack->top()) << "\n";
		if(!isnumber(&mainstack->top()))
		{
			char oper8er = mainstack->top()[0];
			mainstack->pop();//clearing the operator to make way for the result
			if(rpncalc::decide(toptwo, &oper8er, mainstack))//this is for 1 operand thingies like square roots 
				return 2;
		}
		else
		{
			toptwo[1] = converttodouble(&mainstack->top());
			mainstack->pop();//got the toptwo
			char oper8er = mainstack->top()[0];
			mainstack->pop();//clearing the operator to make way for the result
			if(rpncalc::decide(toptwo, &oper8er, mainstack))
				return 3;
		}
	}
	return 0;
}

