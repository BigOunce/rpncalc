#ifndef RPNCALC_TEXT_PARSER
#define RPNCALC_TEXT_PARSER

#include <string>
#include <regex>
#include <tuple>
#include <sstream>
#include <stack>

namespace rpncalc
{
	long double converttodouble(std::string * input);
	//This converts the input string number to a long double

	std::string converttostring(long double input);
	//this converts the input long double to a string
	
	int stackextract(std::stack<std::string> * mainstack, char** argv, int argc);
	//this puts the argv onto the stack
	
	int workthrough(std::stack<std::string> * mainstack); //works through the stack leaving one element left

	bool isnumber(std::string * input);

};
#endif //RPNCALC_TEXT_PARSER
