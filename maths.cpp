#include "maths.h"
#include <stack>
#include <string>
#include "textparser.h"

using namespace std;
using namespace rpncalc;

int rpncalc::decide(long double * toptwo, char * oper8er, std::stack<std::string> * mainstack)
{
	switch(*oper8er)
	{
		case '+':
			mainstack->push(rpncalc::converttostring(rpncalc::addition(toptwo)));
			break;
			
		case '-':
			mainstack->push(rpncalc::converttostring(rpncalc::subtraction(toptwo)));
			break;
		
		case 'x':
			mainstack->push(rpncalc::converttostring(rpncalc::multiply(toptwo)));
			break;

		case '/':
			mainstack->push(rpncalc::converttostring(rpncalc::divide(toptwo)));
			break;
		default:
			return 1;
			break;
	}
	return 0;
};

long double rpncalc::addition(long double * toptwo)
{
	long double temp = toptwo[0] + toptwo[1];
	return temp;
};

long double rpncalc::subtraction(long double * toptwo)
{
	long double temp = toptwo[0] - toptwo[1];
	return temp;
}

long double rpncalc::multiply(long double * toptwo)
{
	long double temp = toptwo[0] * toptwo[1];
	return temp;
}

long double rpncalc::divide(long double * toptwo)
{
	long double temp = toptwo[0] / toptwo[1];
	return temp;
}

