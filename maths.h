#ifndef MATHS_H
#define MATHS_H

#include <stack>
#include <string>

namespace rpncalc
{
	int decide(long double * toptwo, char * oper8er, std::stack<std::string> * mainstack);

	long double addition(long double * toptwo);

	long double subtraction(long double * toptwo);

	long double multiply(long double * toptwo);

	long double divide(long double * toptwo);
}


#endif //MATHS_H
