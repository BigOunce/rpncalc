rpncalc: main.o textparser.o maths.o
	g++ main.o textparser.o maths.o -o rpncalc
main.o: main.cpp
	g++ -c main.cpp
textparser.o: textparser.cpp textparser.h
	g++ -c textparser.cpp 
maths.o:
	g++ -c maths.cpp
clean:
	rm *.o rpncalc
