#include "textparser.h"
#include <iostream>
#include <string>
#include <stack>

using namespace rpncalc;
using namespace std;

int main(int argc, char** argv)
{
	stack<string> * mainstack = new stack<string>(); //the stack where the equation is stored in its component parts

	stackextract(mainstack, argv, argc);

	int exitcode = rpncalc::workthrough(mainstack);

	if(exitcode)
	{
		cout << "Error, program exited with exit code " << exitcode << ". Check manual for more information\n";
	}
	else
	{
		cout << mainstack->top() << "\n";
	}

	return exitcode;
}

